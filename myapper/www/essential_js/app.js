// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'chart.js', 'nvd3'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

   $ionicConfigProvider.tabs.position('bottom');
  
$stateProvider
  .state('sel_proto', {
      url: '/sel_proto',
      templateUrl: 'templates/sel-proto.html',
      controller: 'SelProtoCtrl'
  })

  .state('sel_chart', {
      url: '/sel_chart',
      templateUrl: 'templates/sel-chart.html',
      controller: 'SelChartCtrl'
  })
  .state('phase', {
      url: '/phase',
      templateUrl: 'templates/phase.html',
      controller: 'PhaseCtrl'
  })


  .state('categorie', {
      url: '/categorie',
      templateUrl: 'templates/create-categorie.html',
      controller: 'CreateCategorieCtrl'
  })

  .state('log', {
      url: '/log',
      templateUrl: 'templates/log.html',
      controller: 'LogCtrl'
  })
  .state('addaccount', {
      url: '/addaccount',
      templateUrl: 'templates/addaccount.html',
      controller: 'AddAccountCtrl'
  })


  .state('connection', {
      url: '/connection',
      templateUrl: 'templates/connection.html',
      controller: 'LogCtrl'
  })
 
.state('choose', {
      url: '/choose',
      templateUrl: 'templates/choose.html',
      controller: 'ChooseCtrl'
  })
 
.state('chartvisu', {
      url: '/chartvisu',
      templateUrl: 'templates/chartvisu.html',
      controller: 'ChartVisuCtrl'
  })

.state('cp', {
      url: '/cp',
      templateUrl: 'templates/create-proto.html',
      controller: 'CreateProtoCtrl'
  })
  
  // setup an abstract state for the tabs directive
    .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html'
  })

  .state('tab.home', {
      url: '/home',
      views: {
        'tab-home': {
          templateUrl: 'templates/tab-home.html',
          controller: 'HomeCtrl'
        }
      }
    })
  
  .state('tab.charts', {
      url: '/charts',
      views: {
        'tab-charts': {
          templateUrl: 'templates/tab-charts.html',
          controller: 'ChartsCtrl'
        }
      }
    })

  .state('tab.account', {
    url: '/account',
    views: {
      'tab-account': {
        templateUrl: 'templates/tab-account.html',
        controller: 'AccountCtrl'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/home');

});
