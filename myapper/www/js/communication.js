//THIS IS HOW THE APP WILL COMMUNICATE WITH THE WORLD :D

function strncmp(str1, str2, n) {
  str1 = str1.substring(0, n);
  str2 = str2.substring(0, n);
  return ( ( str1 == str2 ) ? 0 :
                              (( str1 > str2 ) ? 1 : -1 ));
}

function timeSince(date) {

    var seconds = Math.floor((new Date() - date) / 1000);

    var interval = Math.floor(seconds / 31536000);

    if (interval > 1) {
        return interval + " years";
    }
    interval = Math.floor(seconds / 2592000);
    if (interval > 1) {
        return interval + " months";
    }
    interval = Math.floor(seconds / 86400);
    if (interval > 1) {
        return interval + " days";
    }
    interval = Math.floor(seconds / 3600);
    if (interval > 1) {
        return interval + " hours";
    }
    interval = Math.floor(seconds / 60);
    if (interval > 1) {
        return interval + " minutes";
    }
    return Math.floor(seconds) + " seconds";
}

function check_correct_string(string)
{
    for (var i=0; i<string.length; i++) {
    if (string.charAt(i) == 44 || string.charAt(i) == 59 || string.charAt(i) == 47 || string.charAt(i) == 58)
            return(false);
    }
    return (true);
}
function co_create_protos($http, name, email, protos)
{
   $http.get("http://188.166.169.98/mon-quotidien/manage_project.php/?email="+email+"&name="+name+"&protos="+protos)
   .then(function(response) {
       console.log(response.data);
    });
}

function push_data($http, user, fiche_id, data)
{
console.log("GET user_id = "+ user);
$http.get("http://188.166.169.98/v2/store_datas.php/?user="+user+"&fiche_id="+fiche_id+"&data="+JSON.stringify(data))
   .then(function(response) {
       console.log(response.data);
    });
}

function co_create_project($http, name, email)
{
    $http.get("http://188.166.169.98/mon-quotidien/create_datas.php/?email="+email+"&name="+name)
	.then(function(response){
       console.log(response.data);
    });
}

function co_manage_project($http, name, email, right)
{
   $http.get("http://188.166.169.98/mon-quotidien/manage_project.php/?email="+email+"&name="+name+"&right="+right)
   .then(function(response) {
       console.log(response.data);
    });
}

function	store_data($key, $data)
{
	window.localStorage.setItem($key, JSON.stringify($data));
}

function get_data($key)
{
	return (JSON.parse(window.localStorage.getItem($key)));
}

function is_connected($rootScope)
{
	if ($rootScope.online == true)
		return (true);
	else
		return (false);
}
